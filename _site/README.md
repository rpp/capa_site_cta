# Site do Centro de Tecnologia Acadêmica

Este site foi criado utilizando como base o tema [Creative](https://github.com/volny/creative-theme-jekyll) para o [Jekyll](https://jekyllrb.com/). Também utiliza icones da [font awesome](http://fontawesome.io/cheatsheet/).
